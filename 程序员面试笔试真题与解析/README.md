# 程序员面试笔试真题与解析
> 时间：2024年  
> 地点：惠州工业园  

# 第1章 C/C++语言基础知识

## 1.4 字符串

<!--1.4--><details>
<summary>【真题41】从程序健壮性进行分析，下面的FillUserInfo函数和main函数分别存在什么问题？</summary>
  
```
#include <iostream>
#include <string>

#define MAX_NAME_LEN 20

struct USERINFO
{
    int nAge;
    char szName[MAX_NAME_LEN];
}

void FillUserInfo(USERINFO* parUserInfo)
{
    stu::count<<"请输入用户的个数";
    int nCount = 0;
    std::cin>>nCount;

    for(int i=0li<nCount;i++)
    {
        std::count<<"请输入年龄:";
        std::cin>>parUserInfo[i]->nAge;
        std::string strName;
        std::count<<"请输入姓名:";
        std::cin>>strName;
        strcpy(parUserInfo[i].szName,strName.c_str());
    }
}

int main(int argc,char* argv[])
{
    USERINFO arUserInfos[100] = {0};
    FillUserInfo(arUserInfos);
    printf("The first name is:");
    printf(arUserInfos[0].szName);
    printf("\n");
    return 0;
}
```
1）在函数`FillUserInfo`中没有验证`parUserInfo`是否为空就直接使用了。   

2）结构体中`szName`的长度为20,如果名字的长度超过20，数组就会越界会引起段错误。  

3）`arUserInfos`数组的大小为100，如果在输入的用户个数超过100，数组就会越界，也会引发段错误。 
   如果输入用户的个数小于100就会造成空间浪费。

4）如果输入用户的个数为0，那么在`main`函数中输出的第一个用户名将会得到不确定的值。  

5）没有判断输入的年龄是否是一个合法的值（比如负数）。  

6 ）使用`strcpy`可能会导致`szName`数组越界，建议使用`strncpy`。  

7）初始化结构体数组使用的代码为：`USERINFO arUserInfos[100]={0}`;
一般不建议采用这种方式来初始化结构体，比较安全的方法为：`ZeroMemory(arUserInfos,sizeof(arUserInfos));`
或者采用结构体的构造函数来初始化。

</details><!--1.4-->

## 1.5 结构体

<details><summary>【真题45】下述代码定义了一个结构体，如果 Date 的地址是 x ,那么 data[1][5].c 的地址是？</summary>

```
struct Date
{
    char a;
    int b;
    int64_t c;
    char d;
};

Date data[2][10];
```

结构体成员地址对其后，大小等于24字节。  
&data[1][5].c = x+10×24+5×24+1+(3)+4  

</details><!--【真题45】--> 
<!--1.5--> 

## 1.6 指针与引用
<details><summary>【真题48】下述C语言代码中，属于未定义行为的有？</summary>

```
A.int i=0; i=(i++);  
B.char *p="hello"; p[1]='E';  
C.char *p="hello"; char ch=*p++;  
D.int i=0; printf("%d%d\n",i++,i--);  
```

选项B属于未定义行为，p是一个指向 const char 的指针，它指向的东西不能被改变，由于修改了它指向的字符，所以不正确。
</details><!--真题48--> 


<!--1.6--> 

# 第4章 操作系统

## 4.2 进程与线程

<!--4.2-->
<!--真题407--><details><summary>【真题407】轮询任务调度和可抢占式调度有什么区别？</summary>

答：在多任务系统中，在同一时刻通常会有多个任务处于活动状态，操作系统此时就需要对资源进行管理，在任务间实现资源（CPU和内存等）的共享。任务调度是指基于给定时间点、给定时间间隔或者给定执行次数自动执行任务。
轮询任务调度与抢占式任务调度的区别在于抢占式调度中优先级最高的任务可以抢占CPU，而轮询的不能。

具体而言，轮询任务调度的原理是每一次把来自用户的请求轮流分配给内部服务器，从1开始，直到N（内部服务器个数），然后重新开始循环。只有在当前任务主动放弃CPU控制权的情况下（比如任务挂起），才允许其他任务（包括高优先级的任务）控制CPU。其优点是简洁性，它无须记录当前所有连接的状态，所以，它是一种无状态调度，但缺点是不利于后面的请求及时得到响应。

抢占式调度允许高优先级的任务打断当前执行的任务，抢占CPU的控制权。这有利于后面的高优先级的任务也能及时得到响应。但实现相对复杂，并且可能出现低优先级的任务长度得不到调度。
<!--真题407--></details>

<!--真题407--><details><summary>【真题423】程序什么时候应该使用线程？</summary>

答：程序在以下几种情况下使用线程：  
1）耗时的操作使用线程，提高应用程序响应。  
2）并行操作时使用线程，例如C/S架构的服务器端并发响应用户的请求。  
3）在多CPU系统中，使用线程提高CPU利用率。  
4）改善程序结构。一个既长又复杂的进程可以考虑分为多个线程，成为几个独立或半个独立的运行部分，这样的程序会利于理解和修改。  

<!--真题407--></details>

<!--4.2-->

 


# 第x章 模板

## m.n xxx

<!--1.x--><details><summary>【真题xx】</summary>

模板
<!--1.x--></details>

<!--模板--> 

<br>
<br>
<br>
结束