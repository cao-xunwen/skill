<details><summary><h1>第一章：2014下半年嵌入式系统设计师上午试题分析与解答</h1></summary><br>

<details open><summary>试题（71）In computing, a device driver (commonly referred to as simply a driver) is a computer program that operates or controls a particular type of device that is attached to a computer. A driver provides __(71)__ to hardware devices,enabling operating systems and other computer programs to access hardware functions without needing to know precise details of the hardware being used.</summary><br>

A. a software interface   
B. a programming language  
C. a function  
D. an Internal Bus  
- <details><summary>试题（71）分析</summary>

  在计算机中，驱动程序是指操作或者控制一种特定外设的程序。驱动程序提供一种硬件设备的软件操作接口，使得操作系统或者其他程序在不了解硬件的具体情况下可以访问该硬件设备。  
  参考答案： A
  </details>
</details><br><!--试题71-->


<details open><summary><!--以下标题-->
试题（72）__(72)__ occurs when a series of synchronization objects are held in a preemptive system in such a way that no process can move forward.</summary><br><!--以下内容-->

A. Spin Lock     
B. Mutex    
C. Deadlock  
D. Schedule    
- <details><summary>试题（72）分析</summary><!--以下内容-->

  死锁发生在当一系列同步操作发生时，但并没有任何进程可以向前执行的时候。  
  参考答案： C  
  </details>
</details><br><!--试题72-->


<details open><summary><!--以下标题-->
试题（73）Hardware interrupts are triggered by __(73)__ outside the microcontroller.</summary><br><!--以下内容-->

A. user instructions     
B. programs    
C. kernels  
D. peripheral devices      
- <details><summary>试题（73）分析</summary><!--以下内容-->

  硬件中断是由微控制器的外部设备触发而产生的。  
  参考答案： D  
  </details>
</details><br><!--试题73-->


<details open><summary><!--以下标题-->
试题（74）An embedded device is a __(74)__ that has been devised to perform some certain functions.It is dedicated to execute a particular task that might require processors that are powerful.</summary><br><!--以下内容-->

A. peripheral     
B. computer system      
C. kernel     
D. user design        
- <details><summary>试题（74）分析</summary><!--以下内容-->

  一个嵌入式设备是一种用来实现特定功能的系统，可以使得执行某些特定功能的效率更加高效。  
  参考答案： B  
  </details>
</details><br><!--试题74-->



<details open><summary><!--以下标题-->
试题（75）The TCP/IP stack is a complete set of networking protocols. The OSI Model was meant to be a standardized way of connecting devices together, and most protocols have some direct correlation to the OSI Model.The OSI Model has 7 layers,the fourth layer is called __(75)__ .</summary><br><!--以下内容-->

A. physical layer     
B. data link layer        
C. application layer     
D. transport layer          
- <details><summary>试题（75）分析</summary><!--以下内容-->

  TCP/IP协议栈是一种网络协议实现。OSI模型是用来进行设备间连接标准化的一种模型。OSI模型包含7层，第四层是传输层。  
  参考答案： D   
  </details>
</details><br><!--试题74-->
</details><br><!--2014下半年嵌入式系统设计师上午试题分析与解答-->



<details><summary><h1>第3章 2015下半年嵌入式系统设计师上午试题分析与解答</h1></summary><br>

<details open><summary>试题（53）嵌入式实时系统中，有3个主要指标衡量系统的实时性，即响应时间、吞吐量和生存时间，针对这3个指标，下列描述正确的是?</summary><br>

  A 响应时间是计算机从识别一个外部事件到处理完这个事件的时间;  
  B 吞吐量是指系统可以处理的事件总数；  
  C 生存时间是数据有效等待的四间，在这段时间内数据是有效的；  
  D 系统对事件响应时间的长短，可以衡量系统的实时性；  

- <details><summary>试题（53）分析</summary>

  响应时间是计算机从识别一个外部事件到做出响应的时间；  
  吞吐量是指在给定的时间内，系统可以处理的事件总数；  
  生存时间是数据有效等待的时间，在这段时间内数据是有效的。  

  嵌入式实时系统是指系统能够在指定或者确定的时间内，完成系统功能和
  外部或内部、同步或异步事件做出响应的系统。因此，单纯使用绝对的响应时间长短，
  是不能衡量系统的实时性的。

  参考答案 C
  </details>
</details><br>

<details open><summary>试题（55）软件项目实施过程中的里程碑点应在___文档中确定。</summary><br>

  A 软件研制任务书  
  B 软件开发计划  
  C 软件测试计划  
  D 软件研制总结报告  

- <details><summary>试题（55）分析</summary>

  简单来说，里程碑就是在项目过程中管理者或其他利益相关方需要关注的项目状态时间点。
  《软件研制任务书》仅规定任务提出方关注的里程碑，而《软件开发计划》才是规定包括软件研制任务书规定的、
  项目管理者或其他利益相关方关注的和（或）组织规定所需关注的项目状态时间点。项目设置多少里程碑需要在
  项目策划过程中进行计划，并在计划文档中记录，需要利益相关方认可。  

  项目设置里程碑应慎重，不宜太多，一旦设置，就应确保任务完成，否则可能会导致计划的频繁变更。

  参考答案 B
  </details>
</details><br>


<details open><summary>试题（57）(58) 受控库存放的内容包括__(57)__文档和__(58)__代码。</summary><br>

  (57)A 通过评审且评审问题已归零或变更验证已通过，均已完成签署的  
  (58)A 通过了项目规定测试的，或回归测试的，或产品用户认可的     

- <details><summary>试题（57）(58)分析</summary>

  参考答案(57)A (58)A
  </details>
</details><br>

<details open><summary>试题（73）Hardware interrupts are triggered by____outside the microcontroller.</summary><br>

  A user instructions    
  B peripheral devices    
  C kernel    
  D program    

- <details><summary>试题（73）分析</summary>

  嵌入式微控制器的中断触发是由外部的设备所引起的。  
  参考答案 B
  </details>
</details><br>

<details open><summary>试题（74）Digital Signal Processing（DSP），has emerged as an important technology for modern electronic systems.It is a form of____that is one of the newest and hottest fileds,and id considered to be the workhorse of choice for many computational-intensive application.</summary><br>    

  A embedded design      
  B programming      
  C kernel    
  D software      

- <details><summary>试题（74）分析</summary>

  数字信号处理器是目前电子系统的一种关键性技术。它是一种嵌入式系统的设计，
  并且是最新的和最热门的领域之一，在许多密集型计算中都会考虑用数字信号处理器来进行实现。  
  参考答案 A
  </details>
</details><br>

<details open><summary>试题（75）Embedded C is a set of___for the C Programming language by the C Standards committee to address commonality issues that exist between C externsions for different embedded systems.</summary><br>    

  A programming  
  B database    
  C interface    
  D language externsions    

- <details><summary>试题（75）分析</summary>

  嵌入式C语言是一种依据C语言标准的扩展性编程语言，它强调的是不同嵌入式系统在C语言上的一些共性问题。  
  参考答案 D
  </details>
</details><br>

</details><br><!--第3章 2015下半年嵌入式系统设计师上午试题分析与解答-->

<details><summary><h1>第5章 2016下半年嵌入式系统设计师上午试题分析与解答</h1></summary><br>

<details open><summary>试题71~75 A real-time operating system (RTOS) is an operating system intended to serve__(71)___application process data as it comes in,typically without buffering delays. A key__(72)__of a RTOS is the level of its consistency concerning the amount of time it takes to accept and complete an application's task;the variablity is jitter. A hard real-time operating system has __(73)__jitter than a soft real-time operating system. The chief design goal is not high throughput,but rather a guarantee of a soft or hard performance category. A RTOS has an advanced algorithm for scheduling. __(74)__flexibility enables a wider,computer-system orchestration of process priorities,but a real-time OS is more frequently dedicated to a narrow set of applications, Key factors in a real-time OS are minimal__(75)__latency and minimal thread switching latency.</summary><br>

（71）A.normal B.real-time C.user D.kernel  
（72）A.characteristic B.programming C.structure D.computer  
（73）A.equal B.more C.less D.much  
（74）A.Scheduler B.Programming C.Network D.User  
（75）A.language B.network C.interrupt D.computer  
- <details><summary>试题分析</summary>

  一个实时操作系统（RTOS）是一种服务于实时应用处理的操作系统，在进行数据处理中通常没有缓冲延迟。
  /RTOS的一个关键特性是它对完成一个应用任务时候对需要时间的可接受性，其可变性可用抖动来表示。
  /硬实时操作系统比软实时操作系统具有更小的抖动。
  /实时操作系统主要设计的目标不是高的吞吐量，而是如何保证处理的软实时或硬实时要求。
  /RTOS中会有一个高级的调度算法。
  /调度的灵活性可以使得计算机会有一个更灵活的系统优先级处理方法，但是软实时操作系统更加符合比较特定的一些应用。
  /在实时操作系统中的一个关键因素是要有一个最小的中断延迟和线程切换延时。  
  参考答案：
  （71）B （72）A （73）C （74）A （75）C
  </details>
</details><br><!--题1-->

</details><br><!--第5章 2016下半年嵌入式系统设计师上午试题分析与解答-->

<details><summary><h1>第7章 2017下半年嵌入式系统设计师上午试题分析与解答</h1></summary><br>

<details open><summary>试题(71)~(75)An operating system also has to be able to service peripheral__(71)__,such as times,motors,sensors,communication devices,disk,etc./ All of those can request the attention of the OS __(72)__,i.e at the time that they want to use the OS,the OS has to make sure it's ready to service the requests. / Such a request for attention is called an interrupt. /There are two kinds of interrupts: Hardware interrupts and software interrupts. / The result of an interrupt is also a triggering of the processor,so that it jumps to a __(73)__adress. / Examples of cases where software interrupts appear are perhaps a divide by zero,a memory segmentation fault,etc. / So this kind of interrupt is not casued by a hardware event but by a specific machine language operation code. / Many systems have more than one hardware interrupt line,and the hardware manufacturer typically assembles all these interrupt lines in an interrupt__(74)__. / An Interrupt __(75)__ is a piece of hardware that shields the OS from the electronic details of the interrupt lines,so that interrupts can be queued and none of them gets lost.</summary><br>

(71)A.hardware  B.software  C.application  D.processor  
(72)A.synchronously  B.asynchronously  C.simultaneously  D.directly  
(73)A.random  B.pre-specified  C.constant  D.unknown  
(74)A.vector  B.array  C.queue  D.list  
(75)A.Cell  B.Vector  C.Controller  D.Manager  
- <details><summary>试题(71)~(75)分析</summary>
  
  操作系统也要为外围硬件服务，例如定时器、马达、传感器、通信设备、硬盘等。
  /所有这些硬件设备都可以向操作系统发出异步请求，例如当它们想要使用操作系统时，操作系统必须保证已经做好准备对这些请求进行服务。这种请求被称作中断。
  /中断可以被分为两类：硬件中断和软件中断。
  /一个中断的结果是对处理器进行触发，使得处理器跳转到预定义的地址执行。
  /例如，除以0或内存访问段错误等都可以触发软中断。
  /这种中断不是由硬件触发产生，而是由特定的机器语言操作代码产生。
  /很多系统有不止一个中断线，硬件厂商一般会集成这些中断线到一个中断向量中。
  /中断控制器是一块硬件资源，它使得操作系统不必关注中断线的电气特性，也使得所有中断会被队列缓存而不至于丢失。

  参考答案  
  (71)A(72)B(73)B(74)A(75)C
  </details>
<!--第7章 2017下半年嵌入式系统设计师上午试题分析与解答-->
</details><br>

<!--2018下半年嵌入式系统设计师上午试题分析与解答-->
</details><br>
<details><summary><h1>第9章 2018下半年嵌入式系统设计师上午试题分析与解答</h1></summary><br>

<details open><summary>试题（71） An embedded device is an object that contians a __(71)__ comuting system. The system,which is completely enclosed by the object,may or may not be able to connect to the Internet. Embedded systems have externsive applications in consumer,commercial,automotive,industrial and  healthcare markets. It's estimated that over 15 bilion embedded devices have been connected to the Internet, a phenomenon commonly referred to as the __(72)__. Generallly, an embedded device's operating system will only run a single application which helps the device to do its job.Examples of embedded devices include dishwashers,banking ATM machines,routes,point of sale terminals(POS terminals) and cell phones. Devices that can not connect to the Internet,it is called dumb. Embedded devices in complex manufactured products,such as automoblies, are often headless. This simply means that the devices's software does not have a user interface(UI). In such cases,an in-circuit __(73)__ is temporarily installed between the embedded device and an external computer to debug or update the software. Because embedded systems have limited computing __(74)__ and strict power requirements,writing software for embedded devices is a very specialized filed that requires knowledge of both hardware components and __(75)__ .</summary><br>

```
(71) A. programming    B. special-purpose 
     C. user           D. big

(72) A. Internet of Things   B. system 
     C. computer             D. cloud system

(73) A. simulator   B. system 
     C. emulator    D. device

(74) A. resources   B. power 
     C. system      D. user

(75) A. memory      B. operating system 
     C. driver      D. programming
```

- <details><summary>试题71分析</summary>

  嵌入式设备是指一个具有特定计算功能的设备。嵌入式系统由对应的设备组成，该设备可以连接Internet网络，也可以不进行联网。嵌入式系统可以应用到大量的应用中，包括消费类应用、商业应用、自动化应用、工业与健康领域的应用等。据估计，已经有超过150亿的嵌入式设备连接到Internet网络，一般也被称之为IoT。通常，一个嵌入式设备的操作系统仅仅会运行一个单独的应用，例如：洗碗机、银行的ATM取款机、路由器、POS终端设备、手机。连接到Internet的话，它就会被称之为智能终端。假设一个嵌入式设备没有连接到Internet的话，它就会被称之为哑设备。在复杂的工业产品中，例如汽车领域中，很多嵌入式设备是没有界面的。在这种情况下，一般会有一个在线的仿真器，被用于连接嵌入式设备和外部的电脑，以进行嵌入式设备的调试或者软件更新。因为嵌入式设备的计算资源受限，以及具有严格的功耗要求，所以在嵌入式设备上进行软件开发的要求非常严格，需要开发者对嵌入式硬件的基本元素和软件编程都有一定的了解。  
  
  参考答案：(71) B  (72) A  (73) C  (74) A  (75) D
  </details>
</details><br><!--试题71-->

</details><br><!--2018下半年嵌入式系统设计师上午试题分析与解答-->


<details><summary><h1>第11章 2019下半年嵌入式系统设计师上午试题分析与解答</h1></summary><br>

<details open><summary>试题（71）~（75）Edge computing is a __(71) __，open IT architecture that features decentralized processing power，enabling mobile computing and Internet of Things（IoT）technologies. In edge computing, data is processed by the __(72) __ itself or by a local computer or server, rather than being transmitted to a data center. Edge comuputing enables data-stream acceleration, including real-time data processing without latency. It allows smart __(73) __ and devices to respomd to data almost instantaneously, as its being created, eliminating lag time. This is critical for technologies such as self-driving cars, and has equally important benefits for business. Edge computing allows for efficient data processing in that large amounts of data can be processed near the sources, reducing __(74) __ bandwidth usage. This both eliminates costs and ensures that applicatiions can be used effectively in __(75) __ locations. In addition,the ability to process data without ever putting it into a public cloud adds a useful layer of security for sensitive data. </summary><br>

```
(71) A. distributed        B. computer
     C. operating system   D. cloud system

(72) A. computer   B. device   C. I/O  D. server

(73) A. simulator  B. system  C. applications  D. devices

(74) A.internet    B.power    C. systerm D. user

(75) A. memory     B. operating system    C.local    D.remote
```

- <details><summary>试题（71）~（75）分析</summary>

  边缘计算是一种分布式、开发的结构设计，可以实现去中心化的移动计算和物联网技术。在边缘计算中，数据是由设备或者本地的计算机或服务器来进行处理，而不用传输到远端的数据中心。边缘计算可以实现数据流的处理加速，包括无延时的实时数据处理。同时，它也可以支撑智能化应用和设备来快速对数据进行响应。这在一些应用领域如自动驾驶中非常关键，同时对于商业化实现也有很重要的作用。边缘计算中对数据的处理可以靠近数据源，因此可以实现数据的有效处理，降低网络带宽的使用率。这同样对于降低成本、保障应用在远端的实现非常重要。此外，在边缘计算中不用将数据传输到公共云上也对数据的安全性保障起到非常重要的作用。  
  参考答案：(71) A  (72) B  (73) C  (74) A  (75) D
  </details>
</details><br><!--试题（71）~（75）-->

</details><br><!--201x下半年嵌入式系统设计师上午试题分析与解答-->


<details><summary><h1>模板：20xx下半年嵌入式系统设计师上午试题分析与解答</h1></summary><br>

<details open><summary>试题x</summary><br>

- <details><summary>试题x分析</summary>
  </details>
</details><br><!--试题x-->

</details><br><!--201x下半年嵌入式系统设计师上午试题分析与解答-->


<br>
<br>
<br>
结束