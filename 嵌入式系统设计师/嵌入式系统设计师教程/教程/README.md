# 嵌入式系统设计师教程（第2版）

<details><summary>
第1章 计算机系统基础知识</summary>

```
1.1 嵌入式计算机系统概述
1.2 数据表示
1.3 算术运算和逻辑运算
1.4 计算机硬件组成及主要部件
1.5 计算机体系结构
1.6 可靠性与系统性能评测基础知识
```
</details><!--end第1章-->

<details><summary>
第2章 嵌入式系统硬件基础知识</summary>

```
2.1 数字电路基础
2.2 嵌入式微处理器基础
2.3 嵌入式系统的存储体系
2.4 嵌入式系统IO
2.5 定时器和计数器
2.6 嵌入式系统总线及通信接口
2.7 嵌入式Soc
  2.7.1 Virtex系列
  2.7.2 Spartan系列
```
</details><!--end第2章-->

<details><summary>
第3章 嵌入式硬件设计</summary>

```
3.1 嵌入式系统电源管理
3.2 电子电路设计
3.3 Cadence PCB系统设计
```
</details><!--end第3章-->

<details open><summary>
第4章 嵌入式系统软件基础知识</summary>

```
4.1 嵌入式软件基础
  4.1.1 嵌入式系统
  4.1.2 嵌入式软件
  4.1.3 嵌入式软件分类
  4.1.4 嵌入式软件体系结构
  4.1.5 设备驱动层
  4.1.6 嵌入式中间件

4.2 嵌入式操作系统概述
  4.2.1 嵌入式操作系统的分类
  4.2.2 常见的嵌入式操作系统

4.3 任务管理
  4.3.1 多道程序技术
  4.3.2 进程、线程和任务
  4.3.3 任务的实现
  4.3.4 任务的调度
  4.3.5 实时系统调度
  4.3.6 任务间的同步与互斥
  4.3.7 任务间通信

4.4 存储管理
  4.4.1 存储管理概述
  4.4.2 实模式与保护模式
  4.4.3 分区存储管理
  4.4.4 地址映射
  4.4.5 页式存储管理
  4.4.6 虚拟存储管理

4.5 设备管理
  4.5.1 设备管理基础
  4.5.2 IO控制方式
  4.5.3 IO软件

4.6 文件系统
  4.6.1 嵌入式文件系统概述
  4.6.2 文件和目录
  4.6.3 文件系统的实现
  4.6.4 典型嵌入式文件系统介绍

4.7 嵌入式数据库
  4.7.1 嵌入式系统对数据库的特殊要求
  4.7.2 典型嵌入式数据库介绍
```
</details><!--end第4章-->

<details open><summary>
第5章 嵌入式系统设计与开发</summary>

```
5.1 嵌入式软件开发概述
  5.1.1 嵌入式应用开发的过程
  5.1.2 嵌入式软件开发的特点
  5.1.3 嵌入式软件开发的挑战

5.2 嵌入式软件开发环境
  5.2.1 宿主机和目标机
  5.2.2 嵌入式软件开发工具
  5.2.3 集成开发环境

5.3 嵌入式软件开发
  5.3.1 嵌入式平台选型
  5.3.2 软件设计
  5.3.3 特性设计技术
  5.3.4 嵌入式软件的设计约束
  5.3.5 编码
  5.3.6 下载和运行

5.4 嵌入式软件移植
  5.4.1 无操作系统的软件移植
  5.4.2 有操作系统的软件移植
  5.4.3 应用软件的移植
```
</details><!--end第5章-->

<details open><summary>
第6章 嵌入式程序设计</summary>

```
6.1 程序语言设计基础

6.2 汇编语言程序设计

6.3 C程序设计基础
  6.3.1 C程序基础
  6.3.2 函数
  6.3.3 存储管理
  6.3.4 指针
  6.3.5 栈与队列
  6.3.6 C程序内嵌汇编

6.4 C++程序设计基础
  6.4.1 面向对象基本概念
  6.4.2 C++程序基础
  6.4.3 类与对象
  6.4.4 继承与多态
  6.4.5 异常处理
  6.4.6 类库
```
</details><!--end第6章-->

<details open><summary>
第7章 嵌入式系统的项目开发与维护知识</summary>

```
7.1 系统开发过程和项目管理
  7.1.1 系统生存周期
  7.1.2 过程模型
  7.1.3 过程评估
  7.1.4 工具与环境
  7.1.5 项目管理
  7.1.6 质量保证

7.2 系统分析知识
  7.2.1 系统需求的定义
  7.2.2 需求分析的基本任务
  7.2.3 需求建模

7.3 系统设计知识
  7.3.1 系统概要设计
  7.3.2 系统详细设计
  7.3.3 系统设计原则
  7.3.4 软硬件协同设计方法

7.4 结构化分析与设计方法
  7.4.1 结构化分析方法
  7.4.2 结构化设计方法
  7.4.3 结构化程序设计方法

7.5 面向对象分析与设计方法
  7.5.1 面向对象分析与设计
  7.5.2 UML构造块
  7.5.3 设计模式

7.6 系统实施知识
  7.6.1 软硬件平台搭建
  7.6.2 系统测试
  7.6.3 系统调试

7.7 系统运行与维护
  7.7.1 系统运行管理
  7.7.2 系统维护概述
  7.7.3 系统评价
```
</details><!--end第7章-->

<details open><summary>
第8章 嵌入式系统软件测试</summary>

```
8.1 软件测试概述
  8.1.1 软件测试的定义
  8.1.2 软件测试的发展
  8.1.3 软件测试与软件开发的关系

8.2 嵌入式软件测试技术
  8.2.1 测试过程
  8.2.2 测试方法
  8.2.3 测试类型
  8.2.4 测试工具
  8.2.5 测试环境

8.3 软件测试实践
  8.3.1 面向对象的软件测试
  8.3.2 基于模型的软件测试
  8.3.3 基于模型开发软件的测试
  8.3.4 分布式软件测试
  8.3.5 测试实例
```
</details><!--end第8章-->

<details open><summary>
第9章 嵌入式系统安全性基础</summary>

```
9.1 计算机信息系统安全概述
  9.1.1 信息系统安全
  9.1.2 网络安全
  9.1.3 风险管理

9.2 信息安全基础
  9.2.1 数据加密原理
  9.2.2 数据加密算法
  9.2.3 认证算法

9.3 安全威胁防范
  9.3.1 防治计算机病毒
  9.3.2 认证
  9.3.3 数字签名
  9.3.4 报文摘要
  9.3.5 数字证书

9.4 嵌入式系统安全方案
  9.4.1 智能卡安全技术
  9.4.2 USB-Key技术
  9.4.3 智能终端的安全技术
  9.4.4 行业工控系统安全
```
</details><!--end第9章-->

<details open><summary>
第10章 标准化、信息化与知识</summary>

```
10.1 标准化基础知识
  10.1.1 概述
  10.1.2 信息技术标准化
  10.1.3 标准化组织
  10.1.4 ISO 9000标准简介
  10.1.5 ISO/IEC 15504过程评估标准简介
  10.1.6 嵌入式系统相关标准简介

10.2 信息化基础知识
  10.2.1 概述
  10.2.2 信息化发展趋势
  10.2.3 信息化应用

10.3 知识产权基础知识
  10.3.1 概述
  10.3.2 计算机软件著作权
  10.3.3 计算机软件的商业秘密权
  10.3.4 专利权概述
  10.3.5 企业知识产权的保护
```
</details><!--end第10章-->

<details open><summary>
第11章 嵌入式系统设计案例分析</summary>

```
11.1 嵌入式系统总体设计
  11.1.1 嵌入式系统设计概述
  11.1.2 案例分析

11.2 嵌入式系统硬件设计
  11.2.1 嵌入式系统硬件设计
  11.2.2 嵌入式系统软硬件协同设计
  11.2.3 案例分析

11.3 嵌入式系统应用设计案例
```
</details><!--end第11章-->
<!--end嵌入式系统设计师教程（第2版）-->


<br>
<br>
<br>
结束